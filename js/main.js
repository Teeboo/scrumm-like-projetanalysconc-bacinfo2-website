const menuHamburger = document.querySelector(".menu-hamburger")
const navLinks = document.querySelector(".nav-links")


menuHamburger.addEventListener('click',()=>{
    navLinks.classList.toggle('mobile-menu')
})


function toggleTranslation() {
    var translationElement = document.getElementById("google_translate_element");
    if (translationElement.style.display === "none") {
        translationElement.style.display = "block";
    } else {
        translationElement.style.display = "none";
    }
}