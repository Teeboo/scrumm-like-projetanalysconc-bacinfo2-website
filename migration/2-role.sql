CREATE TABLE `t_ban` (

                         `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                         `userid` int(10) unsigned NOT NULL,
                         `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         PRIMARY KEY (`id`),
                         CONSTRAINT FK_userid1 FOREIGN KEY (userid) REFERENCES user(id),
                         UNIQUE KEY `userid` (`userid`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;