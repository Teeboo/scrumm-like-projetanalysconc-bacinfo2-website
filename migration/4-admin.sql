CREATE TABLE `t_admin` (

                         `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                         `userid` int(10) unsigned NOT NULL,
                         PRIMARY KEY (`id`),
                         CONSTRAINT FK_userid4 FOREIGN KEY (userid) REFERENCES user(id),
                         UNIQUE KEY `userid` (`userid`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;