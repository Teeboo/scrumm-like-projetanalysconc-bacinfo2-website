CREATE TABLE `user` (
                        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                        `username` varchar(255) NOT NULL,
                        `password` varchar(255) NOT NULL,
                        `email` varchar(255) NOT NULL,
                        `lang` VARCHAR(5) NULL,
                        `firstname` varchar(255) NOT NULL,
                        `lastname` varchar(255) NOT NULL,
                        `phone` int(30) DEFAULT NULL,
                        `created` datetime NOT NULL,
                        `updated` datetime DEFAULT NULL,
                        `lastlogin` datetime DEFAULT NULL,
                        `image` VARCHAR(255) NULL,
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `email` (`email`),
                        UNIQUE KEY `username` (`username`)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;
