
CREATE DATABASE BookMyField;
USE BookMyField;

CREATE TABLE `migration` (
                             `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                             `lasttime` bigint(20) unsigned NOT NULL,
                             `filename` varchar(255) NULL,
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user` (
                        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                        `username` varchar(255) NOT NULL,
                        `password` varchar(255) NOT NULL,
                        `email` varchar(255) NOT NULL,
                        `lang` VARCHAR(5) NULL,
                        `firstname` varchar(255) NOT NULL,
                        `lastname` varchar(255) NOT NULL,
                        `phone` int(30) DEFAULT NULL,
                        `created` datetime NOT NULL,
                        `updated` datetime DEFAULT NULL,
                        `lastlogin` datetime DEFAULT NULL,
                        `image` VARCHAR(255) NULL,
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `email` (`email`),
                        UNIQUE KEY `username` (`username`)
)
    ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `t_ban` (

                         `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                         `userid` int(10) unsigned NOT NULL,
                         `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         PRIMARY KEY (`id`),
                         CONSTRAINT FK_userid1 FOREIGN KEY (userid) REFERENCES user(id),
                         UNIQUE KEY `userid` (`userid`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE `reservation` (
                               `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                               `title` varchar(255) NOT NULL,
                               `image` varchar(255) DEFAULT NULL,
                               `desc` varchar(255) NOT NULL,
                               `status` tinyint(1) DEFAULT 0,
                               `address` varchar(255) NOT NULL,
                               `type` varchar(255) NOT NULL,
                               PRIMARY KEY (`id`),
                               UNIQUE KEY `email` (`title`)
)
    ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `reservation_date` (

                                    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                    `terrainId` int(10) unsigned NOT NULL,
                                    `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                    `status` tinyint(1) DEFAULT 0,
                                    `userid` int(10) unsigned NOT NULL,
                                    PRIMARY KEY (`id`),
                                    CONSTRAINT FK_terrainId1 FOREIGN KEY (terrainId) REFERENCES reservation(id),
                                    CONSTRAINT FK_userid2 FOREIGN KEY (userid) REFERENCES user(id)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO reservation (title, `desc`,address,`type`)
VALUES ("Basic grass terrain","Place holder description of terrain","Placeholder Adress","grass");

INSERT INTO reservation (title, `desc`,address,`type`)
VALUES ("Medium clay terrain","Place holder description of a medium terrain","Placeholder Adress","clay");

INSERT INTO reservation (title, `desc`,address,`type`)
VALUES ("Basic hard terrain","Place holder description of terrain","Placeholder Adress","hard");
CREATE TABLE `t_admin` (

                           `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                           `userid` int(10) unsigned NOT NULL,
                           PRIMARY KEY (`id`),
                           CONSTRAINT FK_userid4 FOREIGN KEY (userid) REFERENCES user(id),
                           UNIQUE KEY `userid` (`userid`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;