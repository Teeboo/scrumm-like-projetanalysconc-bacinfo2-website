CREATE TABLE `reservation` (
                        `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                        `title` varchar(255) NOT NULL,
                        `image` varchar(255) DEFAULT NULL,
                        `desc` varchar(255) NOT NULL,
                        `status` tinyint(1) DEFAULT 0,
                        `address` varchar(255) NOT NULL,
                        `type` varchar(255) NOT NULL,
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `email` (`title`)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `reservation_date` (

                         `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                         `terrainId` int(10) unsigned NOT NULL,
                         `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
                         `status` tinyint(1) DEFAULT 0,
                         `userid` int(10) unsigned NOT NULL,
                         PRIMARY KEY (`id`),
                         CONSTRAINT FK_terrainId1 FOREIGN KEY (terrainId) REFERENCES reservation(id),
                         CONSTRAINT FK_userid2 FOREIGN KEY (userid) REFERENCES user(id)

) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO reservation (title, `desc`,address,`type`)
VALUES ("Basic grass terrain","Place holder description of terrain","Placeholder Adress","grass");

INSERT INTO reservation (title, `desc`,address,`type`)
VALUES ("Medium clay terrain","Place holder description of a medium terrain","Placeholder Adress","clay");

INSERT INTO reservation (title, `desc`,address,`type`)
VALUES ("Basic hard terrain","Place holder description of terrain","Placeholder Adress","hard");

