<?php

namespace app\Controllers;

use app\Helpers\Output;

class admin extends Controller
{
    public function AdminDashboard(): void{
        Output::staticRender("navbar");

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$this->model->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Accès non autorisé');
            return;
        }
        Output::staticRender("adminDashboard");
    }

    public function AdminUserList(): void{
        Output::staticRender("navbar");

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$this->model->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Accès non autorisé');
            return;
        }
        $userList = $this->model->getAllUser();
        Output::render("adminUserList",$userList);
    }
    public function AdminTerrainList(): void{
        Output::staticRender("navbar");

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$this->model->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Accès non autorisé');
            return;
        }
        $TerrainList = $this->model->getAllTerrain();
        Output::render("adminTerrainList",$TerrainList);
    }
    public function AdminReservationList(): void{
        Output::staticRender("navbar");

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$this->model->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Accès non autorisé');
            return;
        }
        $ReservationList = $this->model->getAllReservation();
        Output::render("adminReservationList",$ReservationList);

    }
    public function AdminModifyTerrain(string $terrainId): void{
        Output::staticRender("navbar");

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        if(!$this->model->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Accès non autorisé');
            return;
        }
        $terrain = $this->model->GetTerrainById($terrainId);
        
        if($_POST['title'] != $terrain->title){
            ECHO"SHOULD HAPPEN";
            $this->model->updateTerrainByField($terrainId,'title',$_POST['title']);
        }
        if($_POST['desc'] != $terrain->desc){
            $this->model->updateTerrainByField($terrainId,'`desc`',$_POST['desc']);
        }
        if($_POST['address'] != $terrain->address){
            $this->model->updateTerrainByField($terrainId,'address',$_POST['address']);
        }
        $TerrainList = $this->model->getAllTerrain();
        Output::render("adminTerrainList",$TerrainList);
    }
    public function AdminRemoveTerrain(string $terrainId):void {
        Output::staticRender("navbar");

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }

        if(!$this->model->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Accès non autorisé');
            return;
        }

        $this->model->modifyTerrainStatus($terrainId,1);
        Output::render('messageBox', 'Le terrain numéro '.$terrainId.' a été désactivé',"success");
        $TerrainList = $this->model->getAllTerrain();
        Output::render("adminTerrainList",$TerrainList);
    }
    public function AdminReAddTerrain(string $terrainId):void {
        Output::staticRender("navbar");

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }

        if(!$this->model->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Accès non autorisé');
            return;
        }

        $this->model->modifyTerrainStatus($terrainId,0);
        Output::render('messageBox', 'Le terrain numéro '.$terrainId.' a été réactiver',"success");
        $TerrainList = $this->model->getAllTerrain();
        Output::render("adminTerrainList",$TerrainList);
    }
    public function AdminBanUser(string $userId):void {
        Output::staticRender("navbar");

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }

        $this->model->Ban($userId);
        $userList = $this->model->getAllUser();

        Output::render("adminUserList",$userList);

    }
    public function AdminUnBanUser(string $userId):void {

        Output::staticRender("navbar");

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }

        if(!$this->model->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Accès non autorisé');
            return;
        }

        $this->model->UnBan($userId);
        $userList = $this->model->getAllUser();
        Output::render("adminUserList",$userList);

    }
    public function AdminCreateTerrain():void {
        Output::staticRender("navbar");

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }

        if(!$this->model->CheckAdmin($_SESSION['userid'])){
            Output::render('messageBox', 'Accès non autorisé');
            return;
        }

        if(empty($_POST['name']) OR empty($_POST['description'])OR empty($_POST['type'])OR empty($_POST['address'])){
            $TerrainList = $this->model->getAllTerrain();
            Output::render('messageBox', 'Veuillez remplir la totalité du formulaire');
            Output::render("adminTerrainList",$TerrainList);
            return;
        }
        var_dump($_POST);
        $this->model->AddTerrain($_POST['name'],$_POST['description'],$_POST['address'],$_POST['type']);
        $TerrainList = $this->model->getAllTerrain();
        Output::render("adminTerrainList",$TerrainList);
    }
}