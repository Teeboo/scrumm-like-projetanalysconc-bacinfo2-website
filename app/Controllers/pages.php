<?php

namespace app\Controllers;

use app\Helpers\Output;

class pages extends Controller
{

    public function AboutUs(): void{
        output::staticRender('navbar');
        Output::staticRender('Aboutus');
        Output::staticRender('footer');
    }
    public function ContactUs(): void{
        output::staticRender('navbar');
        output::staticRender('ContactUs');
        Output::staticRender('footer');
    }
    public function Homepage(): void{
        output::staticRender('navbar');
        output::staticRender('homepage');
        Output::staticRender('footer');

    }
    public function gallery(): void{
        output::staticRender('navbar');
        output::staticRender('gallery');
        Output::staticRender('footer');

    }
    public function rgpd(): void{
        output::staticRender('navbar');
        output::staticRender('rgpd');
    }
}