<?php

namespace app\Controllers;

use app\Helpers\Output;

class reservation extends Controller
{

    public function reservationPage():void{

        $terrain = $this->model->getAllTerrain();
        Output::staticRender('navbar');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        Output::Render('reservationPage',$terrain);
        Output::staticRender('footer');
    }

    public function reservationTerrain($terrainID):void{
        Output::staticRender('navbar');

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        $timestamp = strtotime($_POST["date"]);
        $date = date("Y-m-d H:i:s", $timestamp);
        $date = date("Y-m-d", $timestamp);

        if(!$this->model->checkIfReservationIsUp($date,$terrainID)){
            $this->model->reserveTerrain($date,$terrainID,$_SESSION['userid']);
            Output::render('messageBox', 'Reservation done on : '.$date,"success");
            Output::staticRender("homepage");
        }else{
            Output::render('messageBox', 'This field is already taken on : '.$date);
            $terrain = $this->model->getAllTerrain();
            Output::Render('reservationPage',$terrain);
        }
    }


}