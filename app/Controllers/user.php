<?php
namespace app\Controllers;

use app\Helpers\Output;
use \app\Models\role;
use stdClass;

class user extends Controller
{

    public function requestCheck(array $input): void{
        if(!$input){
            Output::render('messageBox', 'Veuillez utilisez l\'interface du site uniquement');
        }
    }
    public function homePage(): void{
        output::staticRender('navbar');
    }
    public function profil(): void{
        Self::homePage();
        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        $user= $this->model->getUserById($_SESSION['userid']);
        Output::render('profil',$user);
        Output::staticRender('footer');
    }
    public function SignUp(): void{

        //var_dump($_POST);
        self::requestCheck($_POST);

        if(!is_numeric($_POST['phone']) AND !empty($_POST['phone'])){
            output::staticRender('navbar');
            Output::render('messageBox', 'Veuillez utiliser un format de numéro de téléphone correcte');
            return ;
        }

        Output::staticRender('navbar');

        if(empty($_POST['phone'])){
            $_POST['phone'] = 0;
        }
        if (!empty($_POST['last-name']) && !empty($_POST['first-name']) && !empty($_POST['password']) && !empty($_POST['username'])
            && !empty($_POST['email'])){


                if( is_numeric($_POST['last-name']) OR  is_numeric($_POST['first-name'])){
                    Output::render('messageBox', 'Erreur : Nom et prénom ne peuvent contenir de chiffres');
                    return;
                }


                if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
                    Output::render('messageBox', 'Votre adresse email est incorrecte');
                    return;
                }


                $user_data = [
                    'lastname' => trim($_POST['last-name']),
                    'firstname' => trim($_POST['first-name']),
                    'username' => trim($_POST['username']),
                    'password' => password_hash($_POST['password'], PASSWORD_DEFAULT),
                    'email' => $_POST['email'],
                    'phone' => trim($_POST['phone']),
                ];


                if($this->model->checkIfUsernameExist($user_data['username'])){

                    Output::render('messageBox', 'L\'utilisateur '.$user_data['username'].' existe déja');
                    return ;
                }

                if($this->model->checkIfEmailExist($user_data['email'])){
                    Output::render('messageBox', 'L\'adresse email '.$user_data['email'].' existe déja');
                    return ;
                }

                try {
                    $userid = $this->model->create($user_data);
                } catch (\Exception $e) {
                    Output::render('messageBox', 'Echec de la création de votre compte',);
                    return ;
                }

                if ($userid) {
                    Output::render('messageBox', 'Création de votre compte réussie',"success");
                }
                else{
                    Output::render('messageBox', 'Échec de la création de votre compte');
                }
            }

        else {
            Output::render('messageBox', 'Échec de la création de votre compte, Veuillez remplir entièrement le formulaire');
        }
    }
    public function Login(): void{
        self::requestCheck($_POST);

        if(!empty($_POST['email']) && !empty($_POST['password']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){

            if(!$this->model->checkIfEmailExist($_POST['email'])){
                Output::staticRender('navbar');
                Output::render('messageBox', 'Adresse email inconnue');
            }
            else {
                $user= $this->model->getUser($_POST['email']);

                if(password_verify($_POST['password'], $user->password)) {

                    if($this->model->CheckBan($user->id)){
                        Output::staticRender('navbar');
                        Output::render('messageBox', 'Votre compte est bani, connexion impossible');
                        return;
                    }

                    session_destroy();
                    session_name('BookMyField' . date('Ymd'));
                    session_id(bin2hex(openssl_random_pseudo_bytes(32)));
                    session_start(['cookie_lifetime' => 3600]);

                    $_SESSION['userid'] = $user->id;
                    $_SESSION['lang'] = $user->lang;

                    Output::staticRender('navbar');
                    Output::render('messageBox', 'Réussite de la connexion',"success");
                    $this->model->updatelastlogin($_SESSION['userid']);
                }
                else{
                    Output::staticRender('navbar');
                    Output::render('messageBox', 'Il semble que votre mot de passe n’est pas valide. Veuillez le corriger et essayer à nouveau.');
                }

            }
        }

    }
    public function Logout(): void{

        session_unset();
        session_destroy();
        session_write_close();
        Output::staticRender('navbar');
        Output::staticRender('homePage');
    }
    public function ModifyUser(): void{
        Self::homePage();

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        $Olduser= $this->model->getUserById($_SESSION['userid']);

        if($Olduser->username != $_POST['username']){
            if(!$this->model->checkIfUsernameExist($_POST['username'])){
                $this->model->updateUserByField($_SESSION['userid'],'username',$_POST['username']);
            }
        }

        if($Olduser->email != $_POST['email']){
            if(!$this->model->checkIfEmailExist($_POST['email'])){
                $this->model->updateUserByField($_SESSION['userid'],'email',$_POST['email']);
            }
        }

        if($Olduser->lastname != $_POST['lastname']){
                $this->model->updateUserByField($_SESSION['userid'],'lastname',$_POST['lastname']);
        }

        if($Olduser->firstname != $_POST['firstname']){
                $this->model->updateUserByField($_SESSION['userid'],'firstname',$_POST['firstname']);
        }

        if($Olduser->phone != $_POST['phone']){
                $this->model->updateUserByField($_SESSION['userid'],'phone',$_POST['phone']);
        }
        $user= $this->model->getUserById($_SESSION['userid']);
        Output::render('profil',$user);
    }
    public function UserReservationList(): void{

        Output::staticRender("navbar");

        if(empty($_SESSION['userid'])){
            Output::render('messageBox', 'Veuillez vous connecter');
            return;
        }
        $ReservationList = $this->model->getAllUserReservation($_SESSION['userid']);
        Output::render('userReservationList',$ReservationList);

    }
}
