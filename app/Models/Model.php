<?php

namespace app\Models;

use PDO;
use app\Helpers\DB;

abstract class Model
{

    protected static PDO $connect;

    public function __construct()
    {
        global $connect;

        if (!$connect) {
            $connect = DB::connect();
        }
        self::$connect = $connect;
    }




















}