<?php

namespace app\Models;

class admin extends Model
{

    public function __construct() {
        parent::__construct();
    }

    public static function CheckAdmin(string $id): mixed
    {
        $result = self::$connect->prepare("SELECT count(*) FROM t_admin WHERE userid = ?");
        $result->execute([$id]);
        return $result->fetchColumn();
    }

    public static function getAllTerrain():array{
        $TerrainList = [];
        $result = self::$connect->prepare("SELECT * FROM reservation");
        $result->execute();
        while ($data_tmp = $result->fetchObject()) {
            $TerrainList[] = $data_tmp;
        }
        return $TerrainList;
    }
    public static function getAllUser():array{
        $UserList = [];
        $result = self::$connect->prepare("SELECT * FROM user");
        $result->execute();
        while ($data_tmp = $result->fetchObject()) {
            $UserList[] = $data_tmp;
        }
        return $UserList;
    }
    public static function getAllReservation():array{
        $ReservationList = [];
        $result = self::$connect->prepare("SELECT terrainId,`date`,r.status,userid
                                                        FROM reservation_date as rd
                                                        INNER JOIN reservation as r on rd.terrainId = r.id");
        $result->execute();
        while ($data_tmp = $result->fetchObject()) {
            $ReservationList[] = $data_tmp;
        }
        return $ReservationList;
    }

    public static function modifyTerrainStatus(string $terrainId,string $status):void {
        if($status==1){
            $result = self::$connect->prepare("UPDATE reservation set `status` = 1 WHERE id = ?");
        } else {
            $result = self::$connect->prepare("UPDATE reservation set `status` = 0 WHERE id = ?");
        }

        $result->execute([$terrainId]);
    }

    public static function updateTerrainByField(string $id, string $field, string $value): void
    {
        $result = self::$connect->prepare("UPDATE reservation SET $field = ? WHERE id = ?");
        $result->execute([$value,$id]);
    }

    public static function GetTerrainById(string $id):mixed {
        $result = self::$connect->prepare("SELECT * FROM reservation WHERE id = ?");
        $result->execute([$id]);
        return $result->fetchObject();
    }
    public static function Ban(string $id):void {
        $result = self::$connect->prepare("INSERT INTO t_ban (userid) VALUES (?) ");
        $result->execute([$id]);
    }
    public static function UnBan(string $id):void {
        $result = self::$connect->prepare("DELETE FROM t_ban WHERE userid = ?");
        $result->execute([$id]);
    }
    public static function AddTerrain(string $title,string $description,string $address,string $type):void{

        $result = self::$connect->prepare("INSERT INTO reservation (title,`desc`,address,type) VALUES (?,?,?,?)");
        $result->execute([$title,$description,$address,$type]);



    }
}