<?php

namespace app\Models;
use PDO;
use app\Helpers\Output;

class user extends Model
{

    public function __construct() {
        parent::__construct();
    }

    public static function create(array $data): int
    {
            $insert = self::$connect->prepare(
                "INSERT INTO user (lastname, firstname, username, password, email,phone,created) 
                    VALUES 
                    (?, ?, ?, ?, ?, ?,NOW())");
            $insert->execute(array_values($data));
            if ($insert->rowCount()) {
                return self::$connect->LastInsertId();
            }
            return 0;
}
    public static function checkIfUsernameExist(string $username): int
    {
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user WHERE username = ?");
        $result->execute([$username]);
        return $result->fetchColumn();
    }

    public static function checkIfEmailExist(string $email): int
    {
        $result = self::$connect->prepare("SELECT COUNT(*) FROM user WHERE email = ?");
        $result->execute([$email]);
        return $result->fetchColumn();
    }
    public static function updatelastlogin(string $id): void
    {
        $result = self::$connect->prepare("UPDATE user SET lastlogin = now() WHERE id = ?");
        $result->execute([$id]);
    }

    public static function getUserLanguage(string $languageId): string
    {
        $result = self::$connect->prepare("SELECT languageId FROM user where id = ?");
        $result->execute([$languageId]);
        return $result->fetchColumn();
    }
    public static function getUser(string $email): mixed
    {
        $result = self::$connect->prepare("SELECT * FROM user WHERE email = ?");
        $result->execute([$email]);
        return $result->fetchObject();
    }
    public static function CheckBan(string $id): mixed
    {
        $result = self::$connect->prepare("SELECT count(*) FROM t_ban WHERE userid = ?");
        $result->execute([$id]);
        return $result->fetchColumn();
    }
    public static function getUserById(string $id): mixed
    {
        $result = self::$connect->prepare("SELECT * FROM user WHERE id = ?");
        $result->execute([$id]);
        return $result->fetchObject();
    }
    public static function updateUserByField(string $id, string $field, string $value): void
    {
        $result = self::$connect->prepare("UPDATE user SET $field = ? WHERE id = ?");
        $result->execute([$value,$id]);
    }
    public static function getAllUserReservation(string $userid):array{
        $ReservationList = [];
        $result = self::$connect->prepare("SELECT terrainId,`date`,r.status,userid
                                                        FROM reservation_date as rd
                                                        INNER JOIN reservation as r on rd.terrainId = r.id where userid = ?");
        $result->execute([$userid]);
        while ($data_tmp = $result->fetchObject()) {
            $ReservationList[] = $data_tmp;
        }
        return $ReservationList;
    }
    public static function getTerrainTitleById(string $terrainID): string
    {
        $result = self::$connect->prepare("SELECT title FROM reservation where id = ?");
        $result->execute([$terrainID]);
        return $result->fetchColumn();
    }
}

