<?php

namespace app\Models;

class reservation extends Model
{
    public function __construct() {
        parent::__construct();
    }

    public static function getAllTerrain():array{
        $TerrainList = [];
        $result = self::$connect->prepare("SELECT * FROM reservation where status =0");
        $result->execute();
        while ($data_tmp = $result->fetchObject()) {
            $TerrainList[] = $data_tmp;
        }
    return $TerrainList;
    }

    public static function reserveTerrain(string $date,string $terrainId,string $userid):void{
        $result = self::$connect->prepare("INSERT INTO reservation_date (terrainId,date,userid) VALUES (?,?,?)");
        $result->execute([$terrainId,$date,$userid]);
    }

    public static function checkIfReservationIsUp(string $date,string $terrainId): int
    {
        $result = self::$connect->prepare("SELECT COUNT(*) FROM reservation_date WHERE date = ? AND terrainId = ?");
        $result->execute([$date,$terrainId]);
        return $result->fetchColumn();
    }


}