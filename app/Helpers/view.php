<?php

namespace app\Helpers;
use app\Models\admin;
use app\Models\user;
use app\Models\menu;
use app\Models\course;
class view
{
    public static function messageBox(string $message, string $class = 'danger'): string
    {
        return '<div class=" m-0 text-center alert alert-' . $class . '">' . $message . '</div>';
    }
    public static function homePage():string{
        $body =
            ' 
    <div class="body-about">
        <div class="carousel">
          <ul class="slides">
            <input type="radio" name="radio-buttons" id="img-1" checked />
            <li class="slide-container">
              <div class="slide-image">
                <img src="image/pexels-isabella-mendes-342361.jpg">
              </div>
              <div class="carousel-controls">
                <label for="img-3" class="prev-slide">
                  <span>&lsaquo;</span>
                </label>
                <label for="img-2" class="next-slide">
                  <span>&rsaquo;</span>
                </label>
              </div>
            </li>
            <input type="radio" name="radio-buttons" id="img-2" />
            <li class="slide-container">
              <div class="slide-image">
                <img src="image/pexels-rdne-stock-project-8224718%20(2).jpg">
              </div>
              <div class="carousel-controls">
                <label for="img-1" class="prev-slide">
                  <span>&lsaquo;</span>
                </label>
                <label for="img-3" class="next-slide">
                  <span>&rsaquo;</span>
                </label>
              </div>
            </li>
            <input type="radio" name="radio-buttons" id="img-3" />
            <li class="slide-container">
              <div class="slide-image">
                <img src="image/pexels-rdne-stock-project-8224729%20(1).jpg">
              </div>
              <div class="carousel-controls">
                <label for="img-2" class="prev-slide">
                  <span>&lsaquo;</span>
                </label>
                <label for="img-1" class="next-slide">
                  <span>&rsaquo;</span>
                </label>
              </div>
            </li>
            <div class="carousel-dots">
              <label for="img-1" class="carousel-dot" id="img-dot-1"></label>
              <label for="img-2" class="carousel-dot" id="img-dot-2"></label>
              <label for="img-3" class="carousel-dot" id="img-dot-3"></label>
            </div>
          </ul>
        </div>
        </div>
      <H1>A propos</H1>
      <h2>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Pariatur, non, possimus dolore blanditiis consequatur debitis, earum aliquam nulla laudantium tenetur dolorum nesciunt reiciendis. Rem, quos corrupti tempore nobis veniam impedit animi distinctio quae, hic atque quia? Non officiis perferendis commodi enim libero? Explicabo vitae consequatur libero molestiae, voluptatum sint ullam commodi eos. Quod repudiandae nostrum deserunt exercitationem in omnis iusto, sunt ut blanditiis, cupiditate quibusdam iure praesentium atque dolores, libero deleniti expedita eum totam debitis iste minima fugit ipsum. Molestias, dolorum iure voluptatibus illo officia aliquam eligendi quidem quod sit, totam fuga deleniti iste necessitatibus beatae, omnis et quibusdam debitis laudantium quae qui velit sunt aut doloremque reprehenderit. Neque similique qui consequatur molestias tempore quaerat repudiandae saepe, incidunt eligendi eius.</h2>
      ';
        return $body;
    }
    public static function navbar(): string
    {
        if(!empty($_SESSION['userid'])){
            $admin = new admin();
            $adminbool= $admin->CheckAdmin($_SESSION['userid'],'admin');
        }
        else {
            $adminbool = 0;
        }

        $body =
            '       
            <nav class="navbar">
                    <a href="index.php?view=api/pages/Homepage/"><img src="image/Navbar/logo (2).png" class="logo"></a>
                    <div class="nav-links">
                    <ul>
                      ';
        if(empty($_SESSION['userid'])){
            $body .='<li><a class="modal-login" href="#" data-bs-toggle="modal" data-bs-target="#modal-login">Connexion</a></li>
                      <li><a class="modal-SignUp" href="#" data-bs-toggle="modal" data-bs-target="#modal-SignUp">Inscription</a></li> ';
        }else{
            $body .='   <li><a class="" href="index.php?view=api/user/profil/">Profil</a></li>
                        <li><a class="" href="index.php?view=api/user/UserReservationList">Mes réservations</a></li>
                        <li><a class="" href="index.php?view=api/user/Logout/">Déconnexion</a></li>';
        }
        $body .='
                                                                             
                    </ul>
                    </div>';
        if(empty($_SESSION['userid'])){
            $body .='<img src="image/menu-btn.png" alt="menu hamburger" class ="menu-hamburger" >';
        }else{
            $body .='<img src="image/Navbar/utilisateur.png" alt="menu hamburger" class ="menu-hamburger" >';
        }

        $body .='
                      <div class="nav-links-appear">
                      <ul>';
        if($adminbool){
            $body .='<li><a href="index.php?view=api/admin/AdminDashboard">Admin</a></li>';
        }
        if(!empty($_SESSION['userid'])){
            $body .='<li><a href="index.php?view=api/reservation/reservationPage">Réservation</a></li>';
        }

        $body .='
    <li>
      <a id="flag-link" onclick="toggleTranslation()">
        <img id="flag-img" src="image/Navbar/langue.png" alt="Icône">
      </a>
    </li>
  <div id="google_translate_element">
  </div>
                       <li><a href="index.php?view=api/pages/gallery"><img src="image/Navbar/image.png" alt="Icône"></a></li> 
                        <li><a href="index.php?view=api/pages/ContactUs"><img src="image/Navbar/support.png" alt="Icône"></a></li>
                      </ul>
                      </div>
                    </nav>
                <header></header>
                
            <div class="modal fade " tabindex="-1" id="modal-login" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content ">
                        <div class="">
                            <section class="row nopadding">
                                <div class="col-md-5  p-4 nopadding">
                                    <img src="image/Navbar/Visuel2.png" alt="" class="img-fluid w-100">
                                </div>
                                <div class="col-md-7 ps-5 bg-custom">
                                    <div class="container-contact nopadding">
                                        <div class="pb-4 ">
                                            <h4 class="pb-3 display-6 pt-4 font ">Connexion</h4>
                                            <div class = "pb-5">
                                                <form action = "index.php?view=api/user/login" method="post" enctype="multipart/form-data" >
                                                    <label for="email" class ="font" >E-mail</label>
                                                    <input type="email" id="email" name="email" class="widthform">
                                                    <br>
                                                    <label for="password" class="font">Mot de passe</label>
                                                    <input type="password" id="password" name="password" class="widthform">
                                                    <br>
                                                    <div class ="pt-5">
                                                        <button type="submit" class="btn border-0 btn-custom btn-lg bg-gradient">Connexion</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>   
                    </div>
                </div>
            </div>
            
            <div class="modal fade " tabindex="-1" id="modal-SignUp" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered modal-lg">
                    <div class="modal-content ">
                        <div class="">
                            <section class="row nopadding">
                                <div class="col-md-5  p-4 nopadding">
                                    <img src="image/Navbar/inscription.png" alt="" class="img-fluid w-100">
                                </div>
                                <div class="col-md-7 ps-5 bg-custom">
                                    <div class="container-contact nopadding">
                                        <div class="pb-4 ">
                                            <h4 class="pb-3 display-6 pt-4">Inscription</h4>
                                            <div class = "pb-5">
                                                <form action = "index.php?view=api/user/SignUp" method="post" enctype="multipart/form-data" >
                                                    <label for="email" class ="font" >* E-mail</label>
                                                    <input type="email" id="email" name="email" class="widthform">
                                                         
                                                    <label for="username" class="font">* Pseudo</label>
                                                    <input type="text" id="username" name="username" class="widthform">
                                                                                           
                                                    <label for="last-name" class="font">* Nom</label>
                                                    <input type="text" id="last-name" name="last-name" class="widthform">
                                                    
                                                    <label for="first-name" class="font">* Prénom</label>
                                                    <input type="text" id="first-name" name="first-name" class="widthform">
                                                    
                                                    <label for="password" class="font">* Mot de passe</label>
                                                    <input type="password" id="password" name="password" class="widthform">
                                                    
                                                    <label for="phone" class="font"> Téléphone</label>
                                                    <input type="tel" id="phone" name="phone" class="widthform">
                                                    
                                                    <div class ="pt-5">
                                                        <button type="submit" class="btn border-0 btn-custom btn-lg bg-gradient">Inscription</button>
                                                        
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>   
                    </div>
                </div>
            </div>
        ';
        return $body;
    }
    public static function profil(object $user): string {

        $tbody =  '<div class="container-xl px-4 mt-4">
          <!-- Account page navigation-->
          <hr class="mt-0 mb-4">
          <div class="row">
             
            <div class="spacing-huge">
                  <!-- Account details card-->
                  <div class="card mb-4" style="margin-top: 20px;">
                  
                      <div class="card-header">Données du profil</div>
                      <div class="card-body">
                          <form action = "index.php?view=api/user/ModifyUser" method="post" enctype="multipart/form-data" >
                              <!-- Form Group (username)-->
                              <div class="mb-3">
                                  <label class="small mb-1" for="inputUsername">Pseudo</label>
                                  <input name="username" class="form-control" id="inputUsername" type="text" placeholder="Enter your username" value="'.$user->username.'">
                                  
                              </div>
                              <!-- Form Row-->
                              <div class="row gx-3 mb-3">
                                  <!-- Form Group (first name)-->
                                  <div class="col-md-6">
                                      <label class="small mb-1" for="inputFirstName">Prénom</label>
                                      <input class="form-control" name="firstname" id="inputFirstName" type="text" placeholder="Enter your first name" value="'.$user->firstname.'">
                                  </div>
                                  <!-- Form Group (last name)-->
                                  <div class="col-md-6">
                                      <label class="small mb-1" for="inputLastName">Nom</label>
                                      <input class="form-control" name="lastname" id="inputLastName" type="text" placeholder="Enter your last name" value="'.$user->lastname.'">
                                  </div>
                                </div>
                              <!-- Form Row -->
                              <div class="row gx-3 mb-3">
                                 
                              <!-- Form Group (email address)-->
                              <div class="mb-3">
                                  <label class="small mb-1" for="inputEmailAddress">Adresse e-mail</label>
                                  <input class="form-control" name="email" id="inputEmailAddress" type="email" placeholder="Enter your email address" value="'.$user->email.'">
                              </div>
                              <!-- Form Row-->
                              <div class="row gx-3 mb-3">
                                  <!-- Form Group (phone number)-->
                                      <label class="small mb-1" for="inputPhone">Téléphone</label>
                                      <input class="form-control" name="phone" id="inputPhone" type="tel" placeholder="Enter your phone number" value="'.$user->phone.'">     
                              </div>
                              <!-- Save changes button-->
                              <button type="submit" class="btn btn-primary">Sauvegarder</button>
                          </form>
                      </div>
                  </div>
              </div>
          </div>'
        ;
        return $tbody;
    }
    public static function ContactUs(): string{
        $tbody='
           
        
            <div class="container">
            <h1 class="contact-heading">Contactez-Nous</h1>
            <div class="content">
          <div class="left-side">
            <div class="phone details">
              <i class="fas fa-phone-alt"></i>
              <div class="topic">GSM</div>
              <div class="text-one">+0098 9893 5647</div>
              <div class="text-two">+0096 3434 5678</div>
            </div>
            <div class="email details">
              <div class="topic">E-mail</div>
              <div class="text-one">codinglab@gmail.com</div>
              <div class="text-two">info.codinglab@gmail.com</div>
            </div>
          </div>
          <div class="right-side">
            <div class="topic-text">Envoyez-nous un message</div>
            <p> Si vous avez des questions, nous y répondrons avec plaisir.</p>
            <form action="#">
            <div class="input-box">
              <input type="text" placeholder="Entrez votre nom">
            </div>
            <div class="input-box">
              <input type="text" placeholder="Entrez votre e-mail">
            </div>
            <div class="input-box message-box">
            <textarea placeholder="Entrez votre message"></textarea>
            </div>
            <div class="button">
              <input type="button" value="Envoyer" >
            </div>
          </form>
        </div>
        </div>
      </div>
      ';

        return $tbody;
    }
    public static function Aboutus(): string{
        $tbody='
        ';
        return $tbody;
    }
    public static function reservationPage(array $terrain):string{
        $body='<h6>Réservez votre terrain</h6>';
        foreach ($terrain as $row) {

            $body.='
                    <div class="container2 ">
                        <section class="row nopadding">
                            <div class="col-md-3 nopadding fill"> ';
            if($row->type =="hard")  {
                $body.='<img src="image\terrain\Tenis_default.png" alt="Bootstrap" class ="pb-3 img-fluid" >';
            }
            elseif($row->type =="clay")  {
                $body.='<img src="image\terrain\clay.png" alt="Bootstrap" class ="pb-3 img-fluid" >';
            }
            else{
                $body.='<img src="image\terrain\grass.jpg" alt="Bootstrap" class ="pb-3 img-fluid" >';
            }

            $body.='</div>
                            <div class="col-md-1"></div>
                            <div class="col-md-8">
                               <div class="card cardSizing ">
                                  <div class="card-body">
                                     <h4 class="text-center font ">'.$row->title.'</h4>                                           
                                      <h3 class="font pt-4">Description :</h3>
                                      <p class="pt-4 font">'.$row->desc.'</p>
                                      <p class="pt-4 font">'.$row->address.'</p>
                                      <form action = "index.php?view=api/reservation/reservationTerrain/'.$row->id.'" method="post" enctype="multipart/form-data" >                         
                                            <button type="submit" class="btn border-0 btn-success btn-lg bg-gradient">Reservation</button>
                                            <label for="date">Date :</label>
                                            <input type="date" id="date" name="date" value="2023-07-22" min="2023-07-22" max="2030-1-1">
                                      </form>
                                  </div>
                               </div>
                            </div>           
                        </section>
                    </div>  
                    </div>       
                    ';

        }

        return $body;

    }
    public static function adminDashboard(): string {
        $tbody = '
            <div class="main--content">
           
             <div class="header--wrapper">
                    <div class="header-title">
                        <span>Primary</span>
                        <h2>Dashboard</h2>
                    </div>
                    <div class="user-info">
                         <div class="search-box">
                            <i class="fa-solid fa-magnifying-glass"></i>
                            <input type="text" palceholder="Search">
                        </div>
                        <img src="image/admin.jpg" alt=""/>
                    </div>
                     </div>';

        $sidebar = ' <div class="sidebar">
                <div class="logo"></div>
                <ul class="menu">
                    <li>
                    <a href="#">
                    <i class="fas fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                    </a>
                    </li>
                  <li>
                    <a href="index.php?view=api/admin/AdminUserList">
                    <i class="fa-solid fa-users"></i>
                    <span>Users</span>
                    </a>
                  </li>
                  <li>
                    <a href="index.php?view=api/admin/AdminReservationList">
                    <i class="fa-solid fa-users"></i>
                    <span>Liste de réservation</span>
                    </a>
                  </li>
                  <li>
                    <a href="index.php?view=api/admin/AdminTerrainList">
                    <i class="fas fa-star"></i>
                    <span>Terrains</span>
                    </a>
                  </li>
                 
                  <li class ="logout">
                        <a href="index.php?view=api/user/Logout/">
                        <i class="fas fa-sign-out-alt"></i>
                        <span>Sign Out</span>
                        </a>
                  </li>
                </ul>
          </div>';

        return $tbody . $sidebar;
    }
    public static function adminUserList(array $user): string{

        $tbody ='';
        $userobject = new user();

        foreach ($user as $row) {
            $ban = $userobject->CheckBan($row->id);
            unset($row->password);
            unset($row->lastlogin);
            unset($row->created);
            if(empty($row->phone)){
                $row->phone = "none";
            }
            $tbody .= '<tr>';
            foreach ($row as $value) {
                if ($value) {
                    $tbody .= '<td>' . $value . '</td>';
                }
            }
            if(!$ban){
                $hyper_link ="<a class=\"btn btn-danger\" role=\"button\" href=\"";
                $hyper_link .= "index.php?view=api/admin/AdminBanUser/".$row->id;
                $hyper_link .="\">Bannir</a>";
            }
            else{
                $hyper_link ="<a class=\"btn btn-primary\" role=\"button\" href=\"";
                $hyper_link .= "index.php?view=api/admin/AdminUnBanUser/".$row->id;
                $hyper_link .="\">Débannir</a>";
            }


            $tbody .= '<td>' . $hyper_link .'</td>';
            $tbody .= '</tr>';
       }

        return'<h2 class ="pb-1"> AdminUserList </h2>
        <table class="table table-hover table-dark table-striped table-responsive table-borderless table-dt" id="users-list">
                           <thead>
                                <tr>
                                    <th>id</th>
                                    <th>username</th>
                                    <th>email</th>
                                    <th>firstname</th>
                                    <th>lastname</th>
                                     <th>phone</th>
                                   <th>created</th>
                                  </tr>
                            </thead>
                            <tbody>
                                ' .  $tbody . '
                            </tbody>
                        </table>';
    }
    public static function adminReservationList(array $user): string{

        $tbody ='';
        $userobject = new user();

        foreach ($user as $row) {
            unset($row->id);
            if($row->status == 0){
                $row->status = "Terrain actif";
            }else{
                $row->status = "Terrain Inactif";
            }
            $tbody .= '<tr>';
            foreach ($row as $value) {
                if ($value) {
                    $tbody .= '<td>' . $value . '</td>';
                }
            }


        }

        return'<h2 class ="pb-1"> AdminUserList </h2>
        <table class="table table-hover table-dark table-striped table-responsive table-borderless table-dt" id="users-list">
                           <thead>
                                <tr>                                  
                                    <th>ID du Terrain</th>
                                    <th>Date de la réservation</th>
                                    <th>Status du terrain</th>
                                    <th>ID du utilisateur</th>
                                  </tr>
                            </thead>
                            <tbody>
                                ' .  $tbody . '
                            </tbody>
                        </table>';
    }
    public static function userReservationList(array $user): string{

        $tbody ='';
        $userobject = new user();

        foreach ($user as $row) {
            unset($row->id);
            unset($row->userid);
            $row->terrainId = $userobject->getTerrainTitleById($row->terrainId);
            if($row->status == 0){
                $row->status = "Terrain actif";
            }else{
                $row->status = "Terrain Inactif";
            }
            $tbody .= '<tr>';
            foreach ($row as $value) {
                if ($value) {
                    $tbody .= '<td>' . $value . '</td>';
                }
            }


        }

        return'<h2 class ="pb-1"> Historique de réservation </h2>
        <table class="table table-hover table-dark table-striped table-responsive table-borderless table-dt" id="users-list">
                           <thead>
                                <tr>                                  
                                    <th>Nom du Terrain</th>
                                    <th>Date de la réservation</th>
                                    <th>Status du terrain</th>                                  
                                  </tr>
                            </thead>
                            <tbody>
                                ' .  $tbody . '
                            </tbody>
                        </table>';
    }
    public static function adminTerrainList(array $terrain): string{


        $body='<h6>Gestion de terrains</h6>';

        foreach ($terrain as $row) {

            $body.='
                    <div class="container2">
                        <section class="row nopadding">
                            <div class="col-md-3 nopadding fill"> ';
            if($row->type =="hard")  {
                $body.='<img src="image\terrain\Tenis_default.png" alt="Bootstrap" class ="pb-3 img-fluid" >';
            }
            elseif($row->type =="clay")  {
                $body.='<img src="image\terrain\clay.png" alt="Bootstrap" class ="pb-3 img-fluid" >';
            }
            else{
                $body.='<img src="image\terrain\grass.jpg" alt="Bootstrap" class ="pb-3 img-fluid" >';
            }

            $body.='</div>
                            <div class="col-md-1"></div>
                            <div class="col-md-8">
                               <div class="card cardSizing ">
                                  <div class="card-body">
                                     <form action = "index.php?view=api/admin/AdminModifyTerrain/'.$row->id.'" method="post" enctype="multipart/form-data" >
                                                <div class="mb-3">
                                                  <label class="small mb-1" for="title">Terrain Title</label>
                                                  <input class="form-control" name="title" id="title" type="text" placeholder="'.$row->title.'" value="'.$row->title.'">
                                                </div>
                                                                                                           
                                               <div class="mb-3">
                                                  <label class="small mb-1" for="desc">Terrain Description</label>
                                                  <input class="form-control" name="desc" id="desc" type="text" placeholder="'.$row->desc.'" value="'.$row->desc.'">
                                              </div>
                                              <div class="mb-3">
                                                  <label class="small mb-1" for="address">Terrain Adress</label>
                                                  <input class="form-control" name="address" id="address" type="text" placeholder="'.$row->address.'" value="'.$row->address.'">
                                              </div>
                                              
                                          <div class="pb-4">
                                            <button type="submit" class="btn btn-primary">Save</button>
                                          </div>
                                      </form>';

            if($row->status == "1"){
                $body.='<a class="btn btn-success" href="index.php?view=api/admin/AdminReAddTerrain/'.$row->id.'" role="button">Ajouter</a>';
            }else {
                $body.='<a class="btn btn-danger" href="index.php?view=api/admin/AdminRemoveTerrain/'.$row->id.'" role="button">Supprimer</a>';
            }
            $body.='          </div>
                               </div>
                            </div>           
                        </section>
                    </div>  
                    </div>       
                    ';
        }
        $body.='<div class="form-container rgpd">
                    <h2>Ajouter Terrain</h2>
                    <form action="index.php?view=api/admin/AdminCreateTerrain" method="POST"> 
                      <div class="form-group">
                        <label for="name" class="form-label">Nom de terrain:</label>
                        <input type="text" id="name" name="name" class="form-control" required>
                      </div>
                      <div class="form-group">
                        <label for="description" class="form-label">Description:</label>
                        <textarea id="description" name="description" class="form-control" rows="4" required></textarea>
                      </div>
                      <div class="form-group">
                        <label for="type" class="form-label">Type of terrain:</label>
                        <select id="type" name="type" class="form-select" required>
                          <option value="grass">Grass</option>
                          <option value="hard">Hard</option>
                          <option value="clay">Clay</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="address" class="form-label">Address:</label>
                        <input type="text" id="address" name="address" class="form-control" required>
                      </div>
                      <button type="submit" class="form-button">Submit</button>
                    </form>
                  </div>';
        return $body;
    }
    public static function gallery():string{
        $body =
            ' 
            <div id="diapo">
        <!----------------img01------------------->
            <div>
              <input type="radio" id="check_1" name="check" class="r_check" checked="checked">
              <label for="check_1" class="on_check">
                <img class="vignette" src="image/Navbar/pexels-anna-shvets-5069204.jpg" alt="">
              </label>
              <img class="big_image" src="image/Navbar/pexels-anna-shvets-5069204.jpg" alt=""></div>
        <!----------------img02------------------->
           
           <div>
              <input type="radio" id="check_2" name="check" class="r_check">
              <label for="check_2" class="on_check">
                <img class="vignette" src="image/Navbar/pexels-dmytro-2694944.jpg" alt="">
              </label>
              <img class="big_image" src="image/Navbar/pexels-dmytro-2694944.jpg" alt=""></div>
         <!----------------img03------------------->       
           <div>
              <input type="radio" id="check_3" name="check" class="r_check">
              <label for="check_3" class="on_check">
                <img class="vignette" src="image/Navbar/pexels-jim-de-ramos-1277397.jpg" alt="">
              </label>
              <img class="big_image" src="image/Navbar/pexels-jim-de-ramos-1277397.jpg" alt="">
            </div>
         <!----------------img04------------------->      
                <div>
              <input type="radio" id="check_4" name="check" class="r_check">
              <label for="check_4" class="on_check">
                <img class="vignette" src="image/Navbar/pexels-matthew-turner-2568551.jpg" alt="">
              </label>
              <img class="big_image" src="image/Navbar/pexels-matthew-turner-2568551.jpg" alt="">
            </div>
        <!----------------img05------------------->        
                <div>
              <input type="radio" id="check_5" name="check" class="r_check">
              <label for="check_5" class="on_check">
                <img class="vignette" src="image/Navbar/pexels-pixabay-209977.jpg" alt="">
              </label>
              <img class="big_image" src="image/Navbar/pexels-pixabay-209977.jpg" alt="">
            </div>
          </div>
     ';
        return $body;
    }
    public static function footer():string {

        $tbody = '<footer>
          <div class="footer-gray">
            <div class="footer-custom">
              <div class="footer-lists">
                <div class="footer-list-wrap">
        
                </div>
                <!--/.footer-list-wrap-->
                <div class="footer-list-wrap">
                  <h7 class="ftr-hdr">Service client</h7>
                  <ul class="ftr-links-sub">
                    <li><a href="index.php?view=api/pages/ContactUs">Support</a></li>
                    
                  </ul>
                </div>
                <!--/.footer-list-wrap-->
                <div class="footer-list-wrap">
                  <h7 class="ftr-hdr">Plan du site</h7>
                  <ul class="ftr-links-sub">
                    </art:content>
                    <li class="ftr-Login"><a href="index.php?view=api/pages/Homepage/"><span class="link ftr-access-my-account">Accueil</span></a></li>                 
                    </art:content>
                      <li class="ftr-Login"><a href="index.php?view=api/pages/gallery"><span class="link ftr-access-my-account">Galerie</span></a></li>
                  </ul>
                </div>
                <!--/.footer-list-wrap-->
              </div>
              <!--/.footer-lists-->
        
              <!--/.footer-email-->
              <div class="footer-social">
                <h7 class="ftr-hdr">Suivez-nous</h7>
                <ul>
                  <li>
                    <a href="https://www.facebook.com/art.com" title="Facebook" onclick="_gaq.push(["_trackSocial", "Facebook", "Follow", "Footer", "undefined", "True"]);">
                      <img width="24" height="24" alt="Like us on Facebook" src="http://cache1.artprintimages.com/images/jump_pages/rebrand/footer/fb.png">
                    </a>
                  </li>
                  <li>
                    <a href="https://plus.google.com/108089796661280870153" title="Google+" onclick="_gaq.push(["_trackSocial", "Facebook", "Follow", "Footer", "undefined", "True"]);">
                      <img width="24" height="24" alt="Follow us on Google+" src="http://cache1.artprintimages.com/images/jump_pages/rebrand/footer/gplus.png">
                    </a>
                  </li>
                  <li>
                    <a href="https://pinterest.com/artdotcom/" target="_blank">
                      <img width="24" height="24" alt="Follow us on Pinterest" src="http://cache1.artprintimages.com/images/jump_pages/rebrand/footer/pin-badge.png">
                    </a>
                  </li>
                  <li>
                    <a target="_blank" href="http://instagram.com/artdotcom/">
                      <img width="24" height="24" alt="Follow us on Instagram" src="http://cache1.artprintimages.com/images/jump_pages/rebrand/footer/instagram-badge.png">
                    </a>
                  </li>
                  <li>
                    <a href="https://www.twitter.com/artdotcom" title="Twitter" onclick="_gaq.push(["_trackSocial", "Facebook", "Follow", "Footer", "undefined", "True"]);">
                      <img width="67" alt="Follow us on Twitter" src="http://cache1.artprintimages.com/images/jump_pages/rebrand/footer/twitter.png">
                    </a>
                  </li>
                </ul>
              </div>
              <!--/.footer-social-->
              <div class="footer-legal">
                <p>&copy; Bookmyfield All Rights Reserved. | <a href="index.php?view=api/pages/rgpd/" rel="nofollow">Privacy Policy</a> | <a href="index.php?view=api/pages/rgpd/" rel="nofollow">Terms of Use</a> | <a href="index.php?view=api/pages/rgpd/" rel="nofollow">Terms of Sale</a></p>
              </div>
        
            </div>
            <!--/.footer-custom-->
          </div>
          <!--/.footer-gray-->
        </footer>';
        return $tbody;

    }
    public static function rgpd(): string {
        $tbody = "
            <div class = 'rgpd'>
            <h1>Politique de confidentialité</h1>
            <p>Une politique de confidentialité (ou privacy policy en anglais) est un document juridique qui décrit comment une organisation collecte, utilise et protège les informations personnelles des utilisateurs lorsqu'ils utilisent ses services ou visitent son site web. C'est une exigence légale dans de nombreux pays, notamment dans le cadre du règlement général sur la protection des données (RGPD) de l'Union européenne.
               La politique de confidentialité fournit aux utilisateurs des informations claires et transparentes sur les pratiques de collecte, de traitement et de stockage des données personnelles par une organisation. Elle précise les types d'informations collectées, les finalités pour lesquelles elles sont utilisées, les tiers avec lesquels elles peuvent être partagées, les mesures de sécurité mises en place pour protéger ces informations, ainsi que les droits des utilisateurs en matière de confidentialité et de contrôle de leurs données.
               Une politique de confidentialité doit être rédigée de manière claire, compréhensible et accessible à tous les utilisateurs. Elle est généralement affichée sur un site web dans une page distincte et accessible facilement, souvent via un lien en pied de page ou dans le menu du site.
               En résumé, la politique de confidentialité est un moyen pour les organisations de communiquer ouvertement avec les utilisateurs sur la manière dont elles gèrent les informations personnelles, et de s'assurer qu'elles respectent les réglementations et les attentes en matière de confidentialité des données..</p>
            <h2>Collecte des informations personnelles</h2>
            <p>
                Nous accordons une grande importance à la confidentialité et à la protection des informations personnelles de nos utilisateurs. Dans le cadre de l'utilisation de notre service [nom du service], nous pouvons collecter certaines informations personnelles vous concernant. Cette politique de confidentialité vise à vous informer sur les types d'informations personnelles que nous collectons, la manière dont nous les utilisons, les tiers avec lesquels nous pouvons les partager, ainsi que les mesures de sécurité que nous avons mises en place pour protéger ces informations.
            
                Les informations personnelles que nous collectons peuvent inclure votre nom, votre adresse e-mail, votre numéro de téléphone, votre adresse postale ou toute autre information que vous choisissez de nous fournir lors de l'inscription ou de l'utilisation de notre service. Nous collectons ces informations dans le but de vous offrir une expérience personnalisée, de vous fournir les services demandés, de vous informer sur les mises à jour importantes de notre service et de vous contacter si nécessaire.
            
                Nous ne collectons que les informations personnelles qui sont nécessaires à la fourniture de nos services et nous nous engageons à les traiter de manière légale, équitable et transparente. Nous ne partagerons vos informations personnelles qu'avec des tiers dans les cas suivants : si cela est nécessaire pour répondre à vos demandes, si cela est requis par la loi ou si vous y avez expressément consenti.
            
                Nous mettons en œuvre des mesures de sécurité techniques et organisationnelles appropriées pour protéger vos informations personnelles contre tout accès non autorisé, toute divulgation, toute altération ou destruction. Nous nous engageons à conserver vos informations personnelles uniquement aussi longtemps que nécessaire pour atteindre les objectifs pour lesquels elles ont été collectées, sauf si la loi l'exige autrement.
            
                Nous respectons vos droits en matière de confidentialité et vous disposez du droit d'accéder, de corriger, de mettre à jour ou de supprimer les informations personnelles que nous détenons à votre sujet. Si vous souhaitez exercer ces droits ou si vous avez des questions ou des préoccupations concernant notre politique de confidentialité ou nos pratiques de collecte de données, veuillez nous contacter à [adresse e-mail de contact]. Nous examinerons et répondrons à votre demande dans les délais prévus par la loi applicable.
            
                En utilisant notre service, vous consentez à la collecte et à l'utilisation de vos informations personnelles conformément à cette politique de confidentialité. Nous nous réservons le droit de modifier cette politique de confidentialité à tout moment, en publiant une version mise à jour sur notre site web. Nous vous recommandons de consulter régulièrement cette politique pour prendre connaissance des éventuelles modifications.
            
                Veuillez noter que ce texte est un exemple et vous devrez l'adapter en fonction des spécificités de votre service et de votre entreprise. Assurez-vous de consulter un professionnel du droit pour vous assurer que votre politique de confidentialité est conforme aux lois et réglementations applicables dans votre juridiction.
            </p>
            <h2>Utilisation des informations</h2>
            <p>
                Les informations personnelles que vous nous fournissez sont utilisées dans le strict respect des lois et réglementations en vigueur ainsi que des principes de confidentialité. Nous nous engageons à utiliser vos informations personnelles de manière responsable et sécurisée, et nous ne les partagerons qu'avec des tiers dans les situations prévues par cette politique de confidentialité ou si vous y avez consenti.
            
                    Nous utilisons les informations personnelles que nous collectons dans le but de fournir, d'améliorer et de personnaliser nos services. Ces utilisations peuvent inclure :
            
                Fourniture du service : Nous utilisons vos informations personnelles pour vous fournir les services que vous avez demandés, tels que la création d'un compte, le traitement des paiements, la livraison de produits ou la mise en place de fonctionnalités spécifiques.
            
                    Communication avec vous : Nous utilisons vos informations personnelles pour vous contacter, répondre à vos demandes, vous informer sur les mises à jour de nos services, les nouvelles fonctionnalités ou les offres spéciales. Nous pouvons vous envoyer des e-mails, des notifications push ou des messages similaires liés à votre utilisation de nos services.
            
                    Amélioration de nos services : Nous utilisons les informations personnelles pour comprendre comment nos utilisateurs interagissent avec nos services, analyser les tendances, effectuer des études de marché et améliorer continuellement nos produits et notre expérience utilisateur.
            
                    Personnalisation de l'expérience : Nous utilisons vos informations personnelles pour personnaliser votre expérience en vous proposant du contenu, des offres ou des recommandations adaptées à vos préférences, basées sur votre utilisation antérieure de nos services.
            
                Respect des obligations légales : Dans certains cas, nous pouvons être tenus de traiter vos informations personnelles pour nous conformer aux obligations légales, telles que la prévention de la fraude, la protection des droits de propriété intellectuelle ou la coopération avec les autorités compétentes.
            
                Nous ne conservons vos informations personnelles que pendant la durée nécessaire pour atteindre les objectifs pour lesquels elles ont été collectées, sauf si la loi l'exige autrement. Nous mettons en place des mesures de sécurité techniques et organisationnelles appropriées pour protéger vos informations contre tout accès non autorisé, toute divulgation, toute altération ou destruction.
            
                    Votre consentement est important pour nous. En utilisant nos services, vous consentez à notre utilisation de vos informations personnelles conformément à cette politique de confidentialité. Si vous avez des questions ou des préoccupations concernant l'utilisation de vos informations personnelles, veuillez nous contacter à [adresse e-mail de contact]. Nous ferons de notre mieux pour répondre à vos demandes dans les délais prévus par la loi applicable.
            
                
            </p>
            
            <h2>Conservation des données</h2>
            <p>Nous nous engageons à conserver vos données personnelles uniquement pendant la période nécessaire aux fins pour lesquelles elles ont été collectées, conformément aux exigences légales et aux finalités énoncées dans cette politique de confidentialité.
            
                    Nous conservons vos données personnelles aussi longtemps que vous êtes un utilisateur actif de nos services ou aussi longtemps que cela est nécessaire pour vous fournir les services demandés. Si vous décidez de résilier votre compte ou de cesser d'utiliser nos services, nous conserverons vos données personnelles pendant une période raisonnable afin de nous conformer à nos obligations légales, de résoudre d'éventuels litiges, de faire respecter nos accords et de prévenir toute activité frauduleuse.
            
                    Nous mettons en place des mesures de sécurité appropriées pour protéger vos données personnelles contre tout accès non autorisé, toute divulgation, toute altération ou destruction. Cependant, veuillez noter que malgré nos efforts pour assurer la sécurité de vos données, aucune méthode de transmission sur Internet ou de stockage électronique n'est totalement sécurisée. Par conséquent, nous ne pouvons garantir la sécurité absolue de vos données.
            
                Si vous avez des questions sur notre politique de conservation des données ou si vous souhaitez demander la suppression de vos données personnelles de nos systèmes, veuillez nous contacter à [adresse e-mail de contact]. Nous examinerons votre demande conformément aux lois applicables et prendrons les mesures appropriées pour répondre à votre demande dans les délais prévus par la loi.
            
                Veuillez noter que certaines informations peuvent être conservées de manière anonyme ou agrégée, de manière à ne plus pouvoir être associées à une personne identifiable. Ces informations peuvent être utilisées à des fins statistiques ou de recherche, mais ne permettent pas d'identifier directement un individu.
            
                    Nous nous réservons le droit de modifier notre politique de conservation des données à tout moment, en publiant une version mise à jour sur notre site web. Nous vous recommandons de consulter régulièrement cette politique pour prendre connaissance des éventuelles modifications.
            
                Veuillez noter que ce texte est un exemple et vous devrez l'adapter en fonction des spécificités de votre service et de votre entreprise. Assurez-vous de consulter un professionnel du droit pour vous assurer que votre politique de confidentialité est conforme aux lois et réglementations applicables dans votre juridiction.
            
            </p>
            
            <h2>Vos droits</h2>
            <p>Vous avez le droit d'accéder, de corriger et de supprimer les informations personnelles que nous détenons à votre sujet. Vous pouvez exercer ces droits en nous contactant via les coordonnées fournies ci-dessous.</p>
            
            <h2>Contactez-nous</h2>
            <p>Si vous avez des questions ou des préoccupations concernant cette politique de confidentialité, veuillez nous contacter à l'adresse e-mail suivante : [adresse e-mail de contact].</p>
            
            <p>Nous nous réservons le droit de modifier cette politique de confidentialité à tout moment. Les modifications seront publiées sur cette page.</p>
        </div>";
        return $tbody;
    }
}