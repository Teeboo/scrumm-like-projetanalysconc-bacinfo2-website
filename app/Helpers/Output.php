<?php
namespace app\Helpers;
class Output
{


    /**
     * @param string $template
     * @param object|array|string $data
     * @param string $class
     * @return string
     */
    public static function get(string $template, object|array|string $data, string $class = 'danger'): string
    {
        return view::$template($data, $class);
    }

    /**
     * @param string $template
     * @param object|array|string $data
     * @param string $class
     * @return void
     */
    public static function render(string $template, object|array|string $data, string $class = 'danger')
    {
        echo view::$template($data, $class);
    }

    /**
     * @param string $template
     * @return void
     */
    public static function staticRender(string $template)
    {
        echo view::$template();
    }

    /**
     * @param string $view
     * @return void
     * @throws \ReflectionException
     */
    public static function getContent(string $view): void
    {
        // php array declaration
        $exts = ['html', 'php'];
        // use of SUPER GLOBAL variable ($_SERVER)
        $route = parse_url($_SERVER['REQUEST_URI']);
        // use of new str_contains php core function (php 8.0+)
        if (str_contains($route['query'], 'view=api/')) {
            $params = [];
            // useful explode function (split a string by a string)
            $elements = explode('/', rtrim($route['query'], '/'));
            // foreach control structure (alternative syntax : key | value)
            foreach ($elements as $key => $value) {
                if ($key == 1) {
                    // php concatenation
                    $class = 'app\Controllers\\' . ucfirst($value);
                } elseif ($key == 2) {
                    $method = $value;
                } elseif ($key == 0) {
                    // goto next loop iteration
                    continue;
                } else {
                    // array push
                    $params[] = $value;
                }
            }
            if (!empty($class) && !empty($method)) {
                // use object without "use" keyword
                // Reflection API : https://www.php.net/manual/fr/book.reflection.php
                $r = new \ReflectionMethod($class, $method);
                $nbr = $r->getNumberOfParameters();
                if ($nbr != count($params)) {
                    // php core Exception
                    throw new \Exception('Parameters count mismatch');
                }
                // class instantiation (class name can be a variable)
                $controller = new $class();
                // check if method exists
                is_callable($method, true, $callable_name);
                // method call with parameters (specific php syntax)
                $controller->{$callable_name}(...array_values($params));
            }
        } elseif (!empty($view)) {
            foreach ($exts as $ext) {
                // php concatenation
                $complete_path = $view . '.' . $ext;
                // us of core function file_exists
                if (file_exists($complete_path)) {
                    // php include (include_once avoid multiple includes)
                    include_once $complete_path;
                }
            }
        } else {
            // redirection
            header('Location: index.php?view=view/default');
            // end of execution (alias of exit)
            die;
        }
    }
}